<?php


header('X-Accel-Buffering: no');
ini_set('output_buffering', 'Off');
ini_set('output_handler', '');
ini_set('zlib.output_handler', '');
ini_set('zlib.output_compression', 'Off');
ini_set('implicit_flush', 'On');
while (ob_get_level()) {
    ob_end_flush();
}
ob_implicit_flush(true);
header('Content-Type: text/html; charset=UTF-8');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_regex_encoding('UTF-8');


require_once 'vendor/autoload.php';

class Autoloader {
    static public function loader($className) {
        $filename = str_replace('_', '/', $className) . '.php';
        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
spl_autoload_register('Autoloader::loader');

////////////////////////////////////////////
require 'amazon/Tag.php';
require 'amazon/Templating.php';
use amazon\Tag;
use amazon\Templating;

$product = Tag::buildTag("Product");


$sku = Tag::buildTag("SKU", "2222");

$product->addChild($sku);

/////////////////////////////////////////
$standartProductId = Tag::buildTag("StandartProductId");
$Type = Tag::buildTag("Type", "UPC");
$productVal = Tag::buildTag("Value", "743593397606");

$standartProductId->addChild($Type);
$standartProductId->addChild($productVal);
/////////////////////////////////////////
$product->addChild($standartProductId);



$productTaxCode = Tag::buildTag("ProductTaxCode", "test");

$product->addChild($productTaxCode);

/////////////////////////////////////////////
$condition = Tag::buildTag("Condition");
$conditionType = Tag::buildTag("ConditionType", "test");
$conditionNope = Tag::buildTag("ConditionNote", "CONDITION NOTE TEXT");

$condition->addChild($conditionType);
$condition->addChild($conditionNope);
 /////////////////////////////////////////////////////////////////
$product->addChild($condition);


$itemPackageQuontity = Tag::buildTag("ItemPackageQuantity", "1");
$product->addChild($itemPackageQuontity);
/////////////////////////////////////////////////////////////////////
$descriptionData = Tag::buildTag("DescriptionData");

$brand = Tag::buildTag("Brand", "KYB Corporation/Get-autoparts");
$description = Tag::buildTag("Description", "KYB AGX manually-adjustable shocks and struts allow you to design your own ride control performance without special tools, quickly and as often as you want. Enthusiasts can increase individual corner damping rates up to 125% more than the vehicle’s OEM design. For ease of access, depending on vehicle application, the KYB AGX either has an eight position knob or a four position screwdriver slot. However, both offer the same overall capability. One simple adjustment increases both rebound and compression damping. Whether on the street or on a race track, the AGX’s velocity sensitive damping valves instantly react to driving conditions to maintain tire contact and vehicle control. Specifications: Condition: New | Brand: KYB AGX | Type: Nitrogen Gas Charged | Internal Design: Twin Tube | Adjustable: Yes | Extended Length (IN): 16.61 Inch | Compressed Length (IN): 12.99 Inch | Upper Mounting Style: Stem | Lower Mounting Style: Eyelet | Valving Type: Multi-Stage Valving | Rod Finish: Chrome Plated | Rod Material: Steel | Body Color: Red | Body Material: Iron | With Reservoir: No | Includes Dust Shield: No | Includes Hardware: Yes | Includes Boot: No | Manufacturer Part Number: 741009 | Condition: New | Brand: KYB AGX | Type: Nitrogen Gas Charged | Internal Design: Twin Tube | Adjustable: Yes | Extended Length (IN): 16.61 Inch | Compressed Length (IN): 12.99 Inch | Upper Mounting Style: Stem | Lower Mounting Style: Eyelet | Valving Type: Multi-Stage Valving | Rod Finish: Chrome Plated | Rod Material: Steel | Body Color: Red | Body Material: Iron | With Reservoir: No | Includes Dust Shield: No | Includes Hardware: Yes | Includes Boot: No | ");

$bulletPoint1 = Tag::buildTag("BulletPoint", "BulletPoint1");
$bulletPoint2 = Tag::buildTag("BulletPoint", "BulletPoint2");
$bulletPoint3 = Tag::buildTag("BulletPoint", "BulletPoint3");
$bulletPoint4 = Tag::buildTag("BulletPoint", "BulletPoint4");
$bulletPoint5 = Tag::buildTag("BulletPoint", "BulletPoint5");

$shippingWeight = Tag::buildTag("ShippingWeight", "111", ["unitOfMeasure" => "CM"]);
$manufacturer = Tag::buildTag("Manufacturer", "KYB Corporation/Get-autoparts");
$mfrPartNumber = Tag::buildTag("MfrPartNumber", "123842");
$itemType = Tag::buildTag("ItemType", "automotive-dual-shock-kits");
$targetAudience = Tag::buildTag("TargetAudience", "Adults");


$descriptionData->addChild($brand);
$descriptionData->addChild($description);
$descriptionData->addChild($bulletPoint1);
$descriptionData->addChild($bulletPoint2);
$descriptionData->addChild($bulletPoint3);
$descriptionData->addChild($bulletPoint4);
$descriptionData->addChild($bulletPoint5);
$descriptionData->addChild($shippingWeight);
$descriptionData->addChild($manufacturer);
$descriptionData->addChild($mfrPartNumber);
$descriptionData->addChild($itemType);
$descriptionData->addChild($targetAudience);
////////////////////////////////////////////////////
$product->addChild($descriptionData);


$autoAccessory = Tag::buildTag("AutoAccessory");
$productType = Tag::buildTag("ProductType");
$autoAccessory->addChild($productType);

///////////////////////////////////////////////////
$product->addChild($autoAccessory);


$feed = '<?xml version="1.0" encoding="utf-8" ?>';
$feed .= '<AmazonEnvelope xsi:noNamespaceSchemaLocation="amzn-envelope.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
$feed .= '<Header><DocumentVersion>1.01</DocumentVersion>';
$feed .= '<MerchantIdentifier>1234567</MerchantIdentifier></Header>';
$feed .= '<MessageType>Product</MessageType><PurgeAndReplace>false</PurgeAndReplace>';
$feed .= '<Message><MessageID>1</MessageID><OperationType>Update</OperationType>';

$end = "</Message></AmazonEnvelope>";

$templ = new Templating();

$feed .= $templ->renderTag($product) . $end;

$fieldList = array_unique(Templating::$fieldList);

echo implode(" | ", $fieldList);

file_put_contents("amazontest.xml", $feed);

die;
$sellerId = "A37DO03QNQWJOH";
$marketplaceId = "ATVPDKIKX0DER";
$developerAccountNumber = "8676-5241-8294";
$accessKeyId = "AKIAJYCMNTJY3FRD2INA";
$secretKey = "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";

$FeedType = '_POST_PRODUCT_DATA_';


$client = new MCS\MWSClient([
    'Marketplace_Id' => $marketplaceId,
    'Seller_Id' => $sellerId,
    'Access_Key_ID' => $accessKeyId,
    'Secret_Access_Key' => $secretKey,
    'MWSAuthToken' => '' // Optional. Only use this key if you are a third party user/developer
]);

$a = $client->SubmitFeed($FeedType, $feed, $debug = false);

$b = $a;
