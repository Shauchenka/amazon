<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 11/29/17
 * Time: 10:12 AM
 */

namespace amazon;


class Tag
{
    private $name;

    private $value;

    private $options = [];

    private $childs = [];

    /**
     * Tag constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Tag
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return Tag
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * @param array $childs
     * @return Tag
     */
    public function setChilds($childs)
    {
        $this->childs = $childs;

        return $this;
    }

    /**
     * @param Tag $child
     * @return $this
     */
    public function addChild(Tag $child)
    {
        $this->childs[] = $child;

        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addOption($key, $value)
    {
        $this->options[$key] = $value;

        return $this;
    }

    /**
     * @param $tag
     * @param null $val
     * @param array $options
     * @return Tag
     */
    public static function buildTag($tag, $val = null, $options = []){
        $tag = new Tag($tag);
        if(!is_null($val)){
            $tag->setValue(quotemeta($val));
        }
        $tag->setOptions($options);

        return $tag;
    }

}