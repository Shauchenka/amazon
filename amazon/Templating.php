<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 11/29/17
 * Time: 10:14 AM
 */

namespace amazon;


class Templating
{

    public static $fieldList;

    /**
     * @param Tag $tag
     * @return string
     */
    public function renderTag(Tag $tag)
    {
        self::$fieldList[] = $tag->getName();

        $result = "<{$tag->getName()}";

        foreach ($tag->getOptions() as $key => $option){
            $result .= ' ' . $key . '="' . $option . '" ';
        }

        $result .= ">";

        if(count($tag->getChilds())){
            foreach ($tag->getChilds() as $child){
                $result .= $this->renderTag($child);
            }
        }

        $result .= '<![CDATA[' . $tag->getValue() . ']]>';;
        $result .= "</{$tag->getName()}>";

        return $result;
    }
}